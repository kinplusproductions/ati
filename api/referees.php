<?php 
require_once('initialize.php');

$method = $_SERVER['REQUEST_METHOD'];
$table = "referees";
$setup = selectRecord('applicants_setup',[], "status=1",[]);
$application_year = $setup['application_year'];


switch($method){
	case 'GET':
		$u = isset($_GET['u']) ? $_GET['u'] : '' ;
		$count = isset($_GET['count']) ? $_GET['count'] : '' ;
		// $application_year = isset($_GET['a']) ? $_GET['a'] : '' ;
		if($u==2){
			if($count == 1){
				echo json_encode(countRecords($table));
			}else {
				
				echo json_encode(selectRecords($table, [], "application_year=:application_year ORDER BY created_on ASC", ['application_year'=>$application_year]));
			}
		} else{
			echo json_encode([]);
		} 
		// echo json_encode(selectReferences($table, '*', "1 ORDER BY application_no"));
		break;
	case 'POST': 
		$data = json_decode(file_get_contents("php://input"), true); // Get raw posted data

		extract($data);
	
		$ref1 = $referees[0];
		$ref2 = $referees[1];
		$ref1['id'] = uuid();
		$ref2['id'] = uuid();
		$ref1['applicant_id'] = $applicant_id;
		$ref2['applicant_id'] = $applicant_id;
		$setup = selectRecord('applicants_setup',[], "status=1",[]);
		$application_year = $setup['application_year'];
		$ref1['application_year'] = $application_year;
		$ref2['application_year'] = $application_year;

		$column = "id,applicant_id,referee_name,referee_address,referee_phoneno,referee_email,application_year";
		
		$value = ":id, :applicant_id, :referee_name, :referee_address, :referee_phoneno, :referee_email, :application_year";

		// $post_data=[
		// 	'id'=>$id, 
		// 	'applicant_id'=>$applicant_id,'referee_name'=>$referee_name,'referee_address'=>$referee_address,'referee_phoneno'=>$referee_phoneno,'referee_email'=>$referee_email,'application_year'=>$application_year];

		$res1 = insertRecord($table, $column, $value, $ref1);
		$res2 = insertRecord($table, $column, $value, $ref2);
			if ($res1 && $res2) {					
				// echo json_encode('inserted'); 
				echo json_encode([$ref1, $ref2]); 
			} else {
				echo json_encode('Unable to create new record, Please Try Again Later');				
			}	
		break;
		
		break;
	
		case 'PUT': 
			case 'PATCH':
				$data = json_decode(file_get_contents("php://input"), true); // Get raw posted data
				$res = '';
				extract($data);
				switch ($type) {
					case 'referee':
	
		$ref1 = $referees[0];
		$ref2 = $referees[1];
						$column = "referee_name=:referee_name,referee_address=:referee_address,referee_phoneno=:referee_phoneno,referee_email=:referee_email";
						// time_length=:time_length,capacity=:capacity,salvation_experience=:salvation_experience,walk_with_God=:walk_with_God,strength=:strength,weaknesses=:weaknesses,exceptional_ability=:exceptional_ability,work_ability=:work_ability,other_areas=:other_areas,submitted=:submitted";
		
						$update_data1 =['id'=>$ref1['id'], 'referee_name'=>$ref1['referee_name'],'referee_address'=>$ref1['referee_address'], 'referee_phoneno'=>$ref1['referee_phoneno'], 'referee_email'=>$ref1['referee_email']];
						$update_data2 =['id'=>$ref2['id'], 'referee_name'=>$ref2['referee_name'],'referee_address'=>$ref2['referee_address'], 'referee_phoneno'=>$ref2['referee_phoneno'], 'referee_email'=>$ref2['referee_email']];// 'time_length'=>$time_length,'capacity'=>$capacity,'salvation_experience'=>$salvation_experience,'walk_with_God'=>$walk_with_God,'strength'=>$strength,'weaknesses'=>$weaknesses,'exceptional_ability'=>$exceptional_ability,'work_ability'=>$work_ability,'other_areas'=>$other_areas,'submitted'=>$submitted];

						$res1 = updateRecord($table, $column, "id=:id", $update_data1);
						$res2 = updateRecord($table, $column, "id=:id", $update_data2);

					break;

					case 'reference':		
						$submitted=1;
												$column = "applicant_fullname=:applicant_fullname,time_length=:time_length,capacity=:capacity,salvation_experience=:salvation_experience,walk_with_God=:walk_with_God,strength=:strength,weaknesses=:weaknesses,exceptional_ability=:exceptional_ability,work_ability=:work_ability,other_areas=:other_areas,submitted=:submitted";

							$update_data =['id'=>$rf_id,'applicant_fullname'=>$applicant_fullname,'time_length'=>$time_length,'capacity'=>$capacity,'salvation_experience'=>$salvation_experience,'walk_with_God'=>$walk_with_God,'strength'=>$strength,'weaknesses'=>$weaknesses,'exceptional_ability'=>$exceptional_ability,'work_ability'=>$work_ability,'other_areas'=>$other_areas,'submitted'=>$submitted];

						$res = updateRecord($table, $column, "id=:id", $update_data);
						break;  
					default:				
						break;
		}
				echo ($res || ($res1 && $res2)) ? json_encode(['ok' => 1]) : json_encode(['ok' => 0]);
					break;

	default:
		break;
}
?>