<?php 
require_once('initialize.php');

$method = $_SERVER['REQUEST_METHOD'];
$table = "nreferees";
$table2 = "napplicants";

switch($method){
	case 'GET': 
		echo json_encode(selectReferences($table, '*', "1 ORDER BY application_no"));
		break;
	case 'POST': 
		$data = json_decode(file_get_contents("php://input")); // Get raw posted data
		$ref = sanitize_string(json_encode($data->ref));
		$application_no = sanitize_string($data->application_no);
		$referee_id = sanitize_string($data->referee_id);
			$column = "id, application_no, referee_id, ref";
			$value = "(uuid(),'$application_no','$referee_id','$ref')";
			$ref_exist = selectReference($table, "ref", "referee_id='{$referee_id}'");
			if(!$ref_exist){
				$res = insertRecord($table, $column, $value);
				$app = selectApplicant($table2, '*', "application_no='{$application_no}'");
				$app_id = $app->id;
				foreach ($app->referee as $app_ref) {
					if($referee_id === $app_ref->id){
						$app_ref->submitted = 1;
					}
				}
				$app_referee = sanitize_string(json_encode($app->referee));
				updateRecord($table2, "referee='{$app_referee}'", "id='{$app_id}'");
				echo $res ? json_encode(['ok'=>true]) : json_encode(['ok'=>false]);
			} else {
				echo json_encode(['ref_exist'=>true, 'applicant_name' => $ref_exist->applicant_name]);
			}
		
		break;
	case 'DELETE':		
		$id = $_SERVER['QUERY_STRING'];
		$res = deleteRecord($table, "id='{$id}'") ;
		echo $res ? json_encode(selectReferences($table, '*', "1 ORDER BY application_no")) : '';
		break;
	default:
		break;
}
?>