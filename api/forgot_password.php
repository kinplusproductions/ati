<?php 
require_once('initialize.php');

$data = json_decode(file_get_contents("php://input"), true); // Get raw posted data
extract($data);
$npasswd = sha1($npasswd);
$table = "applicants";
$msg = '';
$res = '';

$applicant = selectRecord($table,[], "application_no=:app_no", ['app_no'=>$app_no]);

if($applicant){
	if($phonemail == $applicant['phoneno'] || $phonemail == $applicant['email']){
		$res = updateRecord($table, "passwd=:passwd", "application_no=:app_no", ['app_no'=>$app_no,'passwd'=>$npasswd]);
	}else{
		$msg = "Incorrect Mobile Number or Email: Please Enter Correct Details";
	}
}else{
		$msg = "Incorrect Application Number: Please Enter Correct Details.";
	}

echo $res ? json_encode(selectApplicant($table, "application_no=:app_no", ['app_no'=>$app_no])) : json_encode($msg);

?>