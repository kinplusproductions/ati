<?php 
require_once('initialize.php');

$method = $_SERVER['REQUEST_METHOD'];
$table = 'academic_sessions';
$json_fields = [];

switch($method){
	case 'GET':
		$u = $_GET['u'] ;
		if($u==1){
			echo json_encode(SelectRecords($table, $json_fields, "is_active=1 ORDER BY created_on DESC"));
		} else if($u==2){
			echo json_encode(SelectRecords($table, $json_fields, "1 ORDER BY created_on DESC"));
		}
		break;
	case 'POST':
		$data = json_decode(file_get_contents("php://input"), true); // Get raw posted data
		$id = $data['id'] = uuid();
		$column = "id, name, is_active";
		$value = ":id, :name, :is_active";
    $res = insertRecord($table, $column, $value, $data);
    echo $res ? json_encode(selectRecord($table, $json_fields, "id=:id", ['id' => $id], '*')) : json_encode(['ok' => 0]);			
		break;
	case 'PUT': 
	case 'PATCH': 
		$data = json_decode(file_get_contents("php://input")); // Get raw posted data
		$id = $data->id;
		$res = '';
		$column = "name=:name, is_active=:is_active";
		$update_data=['id'=>$id, 'name'=>$data->name, 'is_active'=>$data->is_active];
		$res = updateRecord($table, $column, "id=:id", $update_data);
		echo $res ? json_encode(['ok' => 1]) : json_encode(['ok' => 0]);
		break;
	case 'DELETE':		
		$id = $_SERVER['QUERY_STRING'];
		$res = deleteRecord($table, "id=:id",['id'=>$id]) ;
		echo $res ? json_encode(['ok' => 1]) : json_encode(['ok' => 0]);
		break;
	default:
		break;
}

?> 