<?php 
require_once('initialize.php');

$method = $_SERVER['REQUEST_METHOD'];
$table = 'course_categories';
$json_fields = [];
$res = '';

switch($method){
	case 'GET':
		$u = isset($_GET['u']) ? $_GET['u'] : '' ;
		$semester = isset($_GET['s']) ? $_GET['s'] : 'First' ;
		$level = isset($_GET['l']) ? $_GET['l'] : 1 ;
		$count = isset($_GET['count']) ? $_GET['count'] : '' ;
		if($u==2){
			if($count == 1){
				echo json_encode(countRecords($table));
				return;
			}
				$where_clause = "level=:level AND semester=:semester ORDER BY created_on ASC";
				$data = ['semester'=>$semester, 'level'=>$level];
				echo json_encode(selectRecords($table, $json_fields,$where_clause , $data));
		} else {
			echo json_encode([]);
		}
		break;
		
	case 'POST':
		$data = json_decode(file_get_contents("php://input"), true); // Get raw posted data
		$id = $data['id'] = uuid();

		$column = "id,course_title,course_code,course_unit,level,semester";

		$value = ":id, :course_title, :course_code, :course_unit, :level, :semester";

        $res = insertRecord($table, $column, $value, $data);

            echo $res? json_encode(selectRecord($table, $json_fields, "id=:id", ['id' => $id], '*')) : json_encode(['ok' => 0]);				
		break;
	case 'PUT': 
		$data = json_decode(file_get_contents("php://input")); // Get raw posted data
		$id = $data->id;
		$res = '';
		// extract($data);
		switch ($data->type) {
			case 'all':				
				$column = "course_title=:course_title,
				course_code=:course_code,
				course_unit=:course_unit,
				level=:level,
				semester=:semester";

				$update_data=[
				'id'=>$id,
				'course_title'=>$data->course_title,
				'course_code'=>$data->course_code,
				'course_unit'=>$data->course_unit,
				'level'=>$data->level,
				'semester'=>$data->semester
				];

				$res = updateRecord($table, $column, "id=:id", $update_data);
				break;			
			default:				
				break;
		}
		
		echo $res ? json_encode(['ok' => 1]) : json_encode(['ok' => 0]);
		break;	
	case 'DELETE':		
		$id = $_SERVER['QUERY_STRING'];
		
        extract(selectRecord($table, [], "id= :id", ['id'=>$id], "*"));

		$res = deleteRecord($table, "id=:id",['id'=>$id]) ;
		echo $res ? json_encode(['ok' => 1]) : json_encode(['ok' => 0]);
		break;
	default:
		break;
}

?>