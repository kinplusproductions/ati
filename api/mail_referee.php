<?php 
require_once('initialize.php');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';

$mail = new PHPMailer();
$data = json_decode(file_get_contents("php://input"), true); // Get raw posted data
// $applicant = $data->applicant;
// $referee = $data->referee;
extract($data);
$res = mail_referee($mail, $applicant, $referee);
echo json_encode($res);

?>