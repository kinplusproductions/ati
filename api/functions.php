<?php
function encodeJson($record, $json_arr){
  foreach ($json_arr as $field) {
    $record[$field] = json_encode($record[$field]);
  }
  return $record;
}

function decodeJson($record, $json_arr){
  foreach ($json_arr as $field) {
    $record[$field] = json_decode($record[$field]);
  }
  return $record;
}

function selectRecords($table, $json_arr=[], $where_clause=1, $data=[], $column="*"){
  global $pdo;
  $sql = "SELECT $column FROM $table WHERE $where_clause ";
  $stmt = $pdo->prepare($sql);
  $stmt->execute($data);
	$res = [];
  while($row = $stmt->fetch()){
    $res[] = decodeJson($row, $json_arr);
  }
	return $res;
}


/* function selectTwoRecords($table, $table2, $json_arr=[], $where_clause=1, $data=[], $column="*",$t1,$t2,$id1){
	global $pdo;
	$sql = "SELECT $column FROM $table AS $t1 INNER JOIN $table2 AS $t2 ON $t1"."."."$id1 = $t2"."."."$id1 WHERE $where_clause ORDER BY"."$id1";
  $stmt = $pdo->prepare($sql);
  $stmt->execute($data);
  $res = [];
  while($row = $stmt->fetch()){
	  $res[] = decodeJson($row, $json_arr);
	}
	return $res;
} */

function selectThreeRecords($table, $table2, $table3, $json_arr=[], $where_clause=1, $data=[], $column="*",$t1,$t2,$t3,$id1){
	global $pdo;
	$sql = "SELECT $column FROM (($table AS $t1 INNER JOIN $table2 AS $t2 ON $t2.$id1 = $t1.$id1) INNER JOIN $table3 AS $t3 ON $t3.$id1 = $t1.$id1) WHERE $where_clause";
	// ."ORDER BY "."$id1";
	$stmt = $pdo->prepare($sql);
	$stmt->execute($data);
	$res = [];
	while($row = $stmt->fetch()){
		$res[] = decodeJson($row, $json_arr);
	}
	return $res;
}

function selectRecord($table, $json_arr=[], $where_clause=1, $data=[], $column="*"){
	global $pdo;
	$sql = "SELECT $column FROM $table WHERE $where_clause LIMIT 1";
	$stmt = $pdo->prepare($sql);
	$stmt->execute($data);
	$res = $stmt->fetch();
	return $res ? decodeJson($res, $json_arr) :false;
}

function selectApplicant($table, $where_clause=1, $data=[], $column="*"){
	global $pdo;
	$sql = "SELECT $column FROM $table WHERE $where_clause LIMIT 1";
	$stmt = $pdo->prepare($sql);
	$stmt->execute($data);
	$res = $stmt->fetch();
	if($res){
		$id = $res['id'];
		$res['referees'] = selectRecords('referees', [], "applicant_id =:id",['id'=>$id]);
	};
	return $res ? $res : false;
}

function selectApplicants($table, $where_clause=1, $data=[], $column="*"){
  global $pdo;
  $sql = "SELECT $column FROM $table WHERE $where_clause ";
  $stmt = $pdo->prepare($sql);
  $stmt->execute($data);
	$res = [];
  while($row = $stmt->fetch()){
	$id = $row['id'];
	$row['referees'] = selectRecords('referees', [], "applicant_id =:id",['id'=>$id]);
	$res[] = $row;
  }
	return $res;
}

function joinRecords($table1, $table2,$column, $on_clause, $where_clause=1, $json_arr=[], $data=[]){
  global $pdo;
  $sql = "SELECT $column FROM $table1 AS t1 INNER JOIN $table2 AS t2 ON $on_clause WHERE $where_clause ";
  $stmt = $pdo->prepare($sql);
  $stmt->execute($data);
	$res = [];
  while($row = $stmt->fetch()){
    $res[] = decodeJson($row, $json_arr);
  }
	return $res;
}

function uuid() {
	return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',

		// 32 bits for "time_low"
		mt_rand(0, 0xffff), mt_rand(0, 0xffff),

		// 16 bits for "time_mid"
		mt_rand(0, 0xffff),

		// 16 bits for "time_hi_and_version",
		// four most significant bits holds version number 4
		mt_rand(0, 0x0fff) | 0x4000,

		// 16 bits, 8 bits for "clk_seq_hi_res",
		// 8 bits for "clk_seq_low",
		// two most significant bits holds zero and one for variant DCE1.1
		mt_rand(0, 0x3fff) | 0x8000,

		// 48 bits for "node"
		mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
	);
}
function insertRecord($table, $column, $value, $data=[]){
  global $pdo;
  $sql ="INSERT INTO $table($column) VALUES ($value) ";
  $stmt = $pdo->prepare($sql);
	return $stmt->execute($data); 
}

function deleteRecord($table, $where_clause, $data=[]){
	global $pdo;
	$sql ="DELETE FROM $table WHERE $where_clause ";
	$stmt = $pdo->prepare($sql);
	return $stmt->execute($data);
}

function updateRecord($table, $column, $where_clause, $data=[]){
	global $pdo;
	$sql ="UPDATE $table SET $column WHERE $where_clause ";
	$stmt = $pdo->prepare($sql);
	return $stmt->execute($data);
}

function countRecords($table){
	global $pdo;
	$sql = "SELECT COUNT(*) AS total FROM $table";
	$stmt = $pdo->prepare($sql);
	$stmt->execute();
	  return $stmt->fetch();
}

function truncateRecord($table){
	global $pdo;
	$sql ="TRUNCATE TABLE $table ";
	$stmt = $pdo->prepare($sql);
	return $stmt->execute();
}

function selectReference($table, $column="*", $where_clause=1){
	global $pdo;
	$sql = "SELECT $column FROM $table WHERE $where_clause LIMIT 1";
	$stmt = $pdo->prepare($sql);
	$stmt ->execute();
	  $row = false;
	if($row = $stmt->fetch()){
	  $row->ref = json_decode($row->ref);
	  }
	  
	return $row;
  }
  
  function selectReferences($table, $column="*", $where_clause=1){
	global $pdo;
	$sql = "SELECT $column FROM $table WHERE $where_clause ";
	$stmt = $pdo->prepare($sql);
	$stmt ->execute();
	  $result = [];
	while($row = $stmt->fetch()){
	  $row->ref = json_decode($row->ref);
	  $result[] = $row;
	}
  
	return $result;
  }

function mail_referee($mail, $applicant, $referee){
	$app_no = $applicant['application_no'];
	// $bd = $applicant->biodata;
	$fname  = strtoupper($applicant['surname'] .', '. $applicant['other_names']);

	$rf_id = $referee['id'];
	$rf_name = $referee['referee_name'];
	$rf_email = $referee['referee_email'];


	$host = 'mail.supremecluster.com';
	$username = 'admissions@ati.livingseed.org';
	$passwd   = 'seed@soa#19';
	$mailfrom = 'admissions@ati.livingseed.org';
	$sendername = 'Peace House Agricultural Training Institute';
	$ref_link = "https://ati.livingseed.org/rf/{$rf_id}";
	$img_link = "https://ati.livingseed.org/img/";
	// $mail->SMTPDebug = 3;                            // Enable verbose debug output

	$mail->isSMTP();                                      // Set mailer to use SMTP
	$mail->Host = $host;                                  // Specify main and backup SMTP servers
	$mail->SMTPAuth = true;   															 // Enable SMTP authentication
	$mail->SMTPKeepAlive = true;  // SMTP connection will not close after each email sent, reduces SMTP overhead
	$mail->Username = $username;                          // SMTP username
	$mail->Password = $passwd;                           // SMTP password
	$mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
	$mail->Port = 465;                                    // TCP port to connect to

	$mail->setFrom($mailfrom, $sendername);

	$mail->isHTML(true);                                  // Set email format to HTML

	$mail->Subject = "Reference requested for {$fname} ({$app_no})";
	
	$mail->addAddress($rf_email, ucwords($rf_name));     // Add a recipient
	$mail->Body = '
		<!DOCTYPE html>
		<html lang="en">
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<style>
			.title{
				text-align: center;
				color: green;
			}
			.row{
				display: flex;
				justify-content: center;
				align-items: center;
				flex-wrap: wrap;
			}
			img {
				max-width: 100%;
			}
			body {
				margin-left: auto;
				margin-right: auto;
				padding-left: 1rem;
      }
      .link {
        color: blue;
      }
		</style>
		<title>Reference Request for '.$fname.'</title>
		</head>
		<body>
		<div class="row">
			<img src="'.$img_link.'ati-logo320x160.png" alt="ATI">
			<div class="title">
				<h4>Peace House <br>  Agricultural Training Institute <br>
				<small>Isarun, Ondo State, Nigeria</small></h4>
			</div> 
			<img src="'.$img_link.'ls-logo.png" alt="LivingSeed">
		</div>
		<h3>Dear '.ucwords($rf_name).',</h3>

		<h4>Applicant Details</h4>
		<b>Applicant Name:</b> '.$fname.' <br>
		<b>Application Number:</b> '. $app_no .' <br>

		<p>This is a request from The Peace House Agricultural Training Institute (ATI, Isarun, Ondo State, Nigeria.</p> 
		<p><b>'. $fname .'</b> has recently made an application to the institute for a comprehensive vocational agricultural training, and has named you as a referee. </p>
		<p>When considering whether to make an offer of place to a particular applicant, our Admissions Team values the opinion of a referee with experience of the applicant.</p> 
		<p>We would, therefore, be very grateful if you would please provide us with a reference for the above applicant, commenting on his/her suitability to undertake the training.</p>
		<p>Please comment on the applicant with all sincerity and faithfulness to God.
		Please make use of the space provided in the following link to give information about the applicant.</p>
		<p><b><a href="'.$ref_link.'">Click here to fill the reference form</a></b></p>
		<p>Alternatively you can copy and paste the following link into your browser directly:  <b class="link">'.$ref_link.'</b></p>
		May I take this opportunity to thank you in advance for your time in preparing this reference.

		<h4><b>ATI Admissions Team</b></h4>
			</body>
		</html>

    ';
		$mail->AltBody = "There is a reference request from The Peace House Agricultural Training Institute (ATI), Isarun, Ondo State. Please follow this link: ".$ref_link." to complete the reference application.";

    if(!$mail->send()) {
        $msg = 'Mail could not be sent.';
        $msg .= 'Mailer Error: ' . $mail->ErrorInfo;
        $mailsent = 0;
    } else {
      $msg = 'Mail Sent Successfully.';
      $mailsent = 1;
    }
    // echo $mail->Body;
		return ['mailsent'=>$mailsent, 'message'=>$msg];
	}
  
function send_double_referee_mail($mail, $applicant, $referee){
	$res = mail_referee($mail, $applicant, $referee[0]);
	$res = mail_referee($mail, $applicant, $referee[1]);
	return $res;
}

?>

