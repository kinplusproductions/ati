<?php 
require_once('initialize.php');

$data = json_decode(file_get_contents("php://input")); // Get raw posted data
$username = $data->username;
$passwd = sha1($data->passwd);
$json_fields = [];
$table = "admins";

$res = selectRecord($table, $json_fields, "username=:username AND passwd=:passwd", ['username' => $username, 'passwd' => $passwd]);

if ($res) {

		echo json_encode($res) ;

}else{
	echo json_encode('Incorrect Login Details: Please Enter Correct Details');
}
?>