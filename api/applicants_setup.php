<?php 
require_once('initialize.php');

$method = $_SERVER['REQUEST_METHOD'];
$table = 'applicants_setup';

switch($method){
	case 'GET':
		$u = isset($_GET['_u']) ? $_GET['_u'] : '' ;
		if($u==1){
			echo json_encode(SelectSingleRecord($table, '*', "status=1"));
		} else if($u==2){
			echo json_encode(SelectRecords($table, '*', "1 ORDER BY created_on DESC"));
		}
		break;
	case 'POST':
		$data = json_decode(file_get_contents("php://input")); // Get raw posted data
		$id = $data->id;
		
		
		if($id){
			$column = "total_questns={$total_questns}, questns_per_page={$questns_per_page}, pass_mark={$pass_mark}, hrs={$hrs}, mins={$mins}, negative_marking={$negative_marking}, category='{$category}', active={$active}";
			$where_clause = "setup_id='{$id}'";
			$res = updateRecord($table, $column, $where_clause);
		} else{
			$column = "setup_id, dept_id, questn_type, total_questns, questns_per_page, pass_mark, hrs, mins, negative_marking, active, category";
			$value = "(uuid(),'{$dept_id}', '{$questn_type}', $total_questns, $questns_per_page, $pass_mark,$hrs,$mins, $negative_marking, $active, '{$category}')";
			$res = insertRecord($table, $column, $value);
		}
		echo $res ? json_encode(SelectRecords($table, '*', "1 ORDER BY created_on DESC")) : '';
		break;
	case 'DELETE':		
		$id = $_SERVER['QUERY_STRING'];
		$res = deleteRecord($table, "setup_id='{$id}'") ;
		echo $res ? json_encode(SelectRecords($table, '*', "1 ORDER BY created_on DESC")) : '';
		break;
	default:
		break;
}

?> 