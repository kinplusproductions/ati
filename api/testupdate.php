<?php

use function PHPSTORM_META\type;

require_once('initialize.php');

// $table1 = 'students';
$table1 = 'napplicants';
$json_fields = ['account','biodata','education','parent','referee','spiritual','action'];

$res = '';
// $app = selectRecords($table2,[],"action like '\"admitted\": \"1\"%' or ");
// $app = selectRecord($table2);
// echo(json_encode( $app) );
$app = selectRecords($table1,$json_fields);

// $app = selectRecords($table1,$json_fields)[0];
// $bd = $app['biodata']; 
//   echo  $dob= $bd->dob;
//     $email= $bd->email;
$ad = [];
foreach ($app as $ap) {
    $id= $ap['id'];
    $bd = $ap['biodata']; 
    $ed = $ap['education']; 
    $dob= $bd->dob;
    $email= $bd->email;
    $otown= $bd->otown;
    $rtown= $bd->rtown;
    $gender= $bd->gender;
    $ostate= $bd->ostate;
    $rstate= $bd->rstate;
    $surname= $bd->surname;
    $ocountry= $bd->ocountry;
    $rcountry= $bd->rcountry;
    $mobile_no= $bd->mobile_no;
    $other_names= $bd->other_names;
    $home_address= $bd->home_address;
    $marital_status= $bd->marital_status;
    $health_challenges= $bd->health_challenges;
    
    $qualification= $ed->qualification;
    $previous_education= $ed->previous_education;
    $qualification_type= $ed->qualification_type;
    // if($ap['action']->admitted == 1){
        // $ap['biodata'] = json_encode($ap['biodata']);

        // $ad=$ap['biodata'];
        // $column = "id,application_no,passwd,account,biodata,education,parent,referee,spiritual,action,submitted,application_year,application_set,certificate,passport,created_on";

    // $update_data = ":id,:application_no,:passwd,:account,:biodata,:education,:parent,:referee,:spiritual,:action,:submitted,:application_year,:application_set,:certificate,:passport,:created_on";

    // where id = id
    // updateRecord()

    $column = "dob=:dob,
    email=:email,
    otown=:otown,
    rtown=:rtown,
    gender=:gender,
    ostate=:ostate,
    rstate=:rstate,
    surname=:surname,
    ocountry=:ocountry,
    rcountry=:rcountry,
    mobile_no=:mobile_no,
    other_names=:other_names,
    home_address=:home_address,
    marital_status=:marital_status,
    health_challenges=:health_challenges,
    qualification=:qualification,
    previous_education=:previous_education,
    qualification_type=:qualification_type";

	$update_data=[
	'id'=>$id,
	'dob'=>$dob,
    'email'=>$email,
    'otown'=>$otown,
    'rtown'=>$rtown,
    'gender'=>$gender,
    'ostate'=>$ostate,
    'rstate'=>$rstate,
    'surname'=>$surname,
    'ocountry'=>$ocountry,
    'rcountry'=>$rcountry,
    'mobile_no'=>$mobile_no,
    'other_names'=>$other_names,
    'home_address'=>$home_address,
    'marital_status'=>$marital_status,
    'health_challenges'=>$health_challenges,
    'qualification'=>$qualification,
    'previous_education'=>$previous_education,
    'qualification_type'=>$qualification_type
	];

   /*  $res=updateRecord($table1, $column, "id=:id", $update_data);
    if ($res) {
        echo 'done';
    }else{echo'not done';} */
    // }
}
// echo $ad;


// echo gettype($ad)


// $n = (int)"1";
// var_dump($n);
/* 
foreach ($app as $app1) {
    $account = json_encode([
      "start"=> true,
      "parent"=> true,
      "upload"=> true,
      "referee"=> true,
      "education"=> true,
      "spiritual"=> true,
    ]);

    $biodata = json_encode([
        "dob"=> $app1['dob'],
        "email"=> $app1['email'],
        "otown"=> $app1['otown'],
        "rtown"=> $app1['rtown'],
        "gender"=> $app1['gender'],
        "ostate"=> $app1['ostate'],
        "rstate"=> $app1['rstate'],
        "surname"=> $app1['surname'],
        "ocountry"=> $app1['ocountry'],
        "rcountry"=> $app1['rcountry'],
        "mobile_no"=> $app1['mobile_no'],
        "other_names"=> $app1['other_names'],
        "home_address"=> $app1['home_address'],
        "marital_stay"=> "Yes",
        "marital_status"=> $app1['marital_status'],
        "health_challenges"=> $app1['health_challenges']
    ]);

    $education = json_encode([
        "qualification"=> $app1['highest_qualification'],
        "previous_education"=> $app1['previous_education'],
        "qualification_type"=> $app1['highest_qualification']
    ]);

    $parent = json_encode([
        "name"=> $app1['parent_name'],
        "address"=> $app1['parent_address'],
        "phoneno"=> $app1['parent_phoneno'],
        "email"=> $app1['parent_email']
    ]);

    $referee = json_encode([
        [
            "id"=> uuid(),
            "name"=> $app1['referee1_name'],
            "email"=> $app1['referee1_email'],
            "address"=> $app1['referee1_address'],
            "phoneno"=> $app1['referee1_phoneno'],
            "submitted"=> 1
        ],
          [
            "id"=> uuid(),
            "name"=> $app1['referee2_name'],
            "email"=> $app1['referee2_email'],
            "address"=> $app1['referee2_address'],
            "phoneno"=> $app1['referee2_phoneno'],
            "submitted"=> 1
        ]
    ]);

    $spiritual = json_encode([
        "church"=> $app1['church'],
        "born_again"=> $app1['born_again'],
        "in_discipleship"=> $app1['in_discipleship'],
        "discipler_name"=> $app1['discipler_name'],
        "mission_burdened"=> $app1['mission_burdened'],
        "salvation_experience"=> $app1['salvation_experience'],
        "mission_burdened_detail"=> $app1['mission_burdened_detail'],
        "salvation_year" => $app1['salvation_year'],
    ]);

    $action = json_encode([
        "admitted"=> $app1['admitted'],
        "declaration"=> $app1['declaration'],
        "interviewed"=> $app1['interviewed'],
        "shortlisted"=> $app1['shortlisted'],
        "rule_abiding"=> $app1['rule_abiding'],
        "accept_admission"=> $app1['accept_admission'],
        "prompt_resumption"=> $app1['prompt_resumption']
    ]);

    $app2 = [
        "id"=> uuid(),
        "application_no"=> $app1['application_no'],
        "passwd"=> $app1["password"],
        "account"=> $account,
        "biodata"=> $biodata,
        "education"=> $education,
        "parent"=> $parent,
        "referee"=> $referee,
        "spiritual"=> $spiritual,
        "action"=> $action,
        "submitted"=> 1,
        "application_year"=> "2019",
        "application_set"=> 1,
        "certificate"=> $app1['certificate'],
        "passport"=> $app1['passport'],
        "created_on"=> $app1['date_created'],
    ];

    $column = "id,application_no,passwd,account,biodata,education,parent,referee,spiritual,action,submitted,application_year,application_set,certificate,passport,created_on";

    $value = ":id,:application_no,:passwd,:account,:biodata,:education,:parent,:referee,:spiritual,:action,:submitted,:application_year,:application_set,:certificate,:passport,:created_on";

    
    $res = insertRecord($table2,$column, $value, $app2 );
    
    // echo json_encode($app2);
} */
?>
  