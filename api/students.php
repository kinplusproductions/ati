<?php 
require_once('initialize.php');

$method = $_SERVER['REQUEST_METHOD'];
$table = 'students';
$json_fields = ['parent','referee','spiritual','action'];
$res = '';

switch($method){
	case 'GET':
		$u = isset($_GET['u']) ? $_GET['u'] : '' ;
		$cur_session = isset($_GET['s']) ? $_GET['s'] : '' ;
		$cur_level = isset($_GET['l']) ? $_GET['l'] : '' ;
		if($u==1){
			// echo json_encode(selectSingleRecord($table, '*', "1 ORDER BY created_on ASC"));
		}
		else if($u==2){
			$where_clause = "cur_session=:cur_session AND cur_level=:cur_level ORDER BY created_on ASC";
			$data = ['cur_session'=>$cur_session, 'cur_level'=>$cur_level];
			echo json_encode(selectRecords($table, $json_fields,$where_clause , $data));
		}
	/* 	else {
			echo json_encode(selectRecords($table, '*', "1 ORDER BY created_on ASC"));
			// print_r(selectRecords($table, '*', "1 ORDER BY created_on ASC"));
		} */
		break;
	case 'PUT': 
		$data = json_decode(file_get_contents("php://input")); // Get raw posted data
		$id = $data->id;
		$res = '';
		// extract($data);
		switch ($data->type) {
			case 'general':				
				$column = "dob=:dob,
				email=:email,
				otown=:otown,
				rtown=:rtown,
				gender=:gender,
				ostate=:ostate,
				rstate=:rstate,
				surname=:surname,
				ocountry=:ocountry,
				rcountry=:rcountry,
				mobile_no=:mobile_no,
				other_names=:other_names,
				home_address=:home_address,
				marital_status=:marital_status,
				health_challenges=:health_challenges,
				qualification=:qualification,
				previous_education=:previous_education,
				qualification_type=:qualification_type";

				$update_data=[
				'id'=>$id,
				'dob'=>$data->dob,
				'email'=>$data->email,
				'otown'=>$data->otown,
				'rtown'=>$data->rtown,
				'gender'=>$data->gender,
				'ostate'=>$data->ostate,
				'rstate'=>$data->rstate,
				'surname'=>$data->surname,
				'ocountry'=>$data->ocountry,
				'rcountry'=>$data->rcountry,
				'mobile_no'=>$data->mobile_no,
				'other_names'=>$data->other_names,
				'home_address'=>$data->home_address,
				'marital_status'=>$data->marital_status,
				'health_challenges'=>$data->health_challenges,
				'qualification'=>$data->qualification,
				'previous_education'=>$data->previous_education,
				'qualification_type'=>$data->qualification_type
				];

				$res = updateRecord($table, $column, "id=:id", $update_data);
				break;
			case 'action':
				$action = json_encode($data->action);
				$res = updateRecord($table, "action=:action", "id=:id", ['id'=>$id, 'action'=>$action]);
				break;
			// case 'referee':
			// 	$referee = json_encode($data->referee);
			// 	$res = updateRecord($table, "referee=:referee", "id=:id", ['id'=>$id, 'referee'=>$referee]);
			// 	break;
			case 'account':
				$account = json_encode($data->account);
				$res = updateRecord($table, "account=:account", "id=:id", ['id'=>$id, 'account'=>$account]);
				break;
			// case 'parent':
			// 	$parent = json_encode($data->parent);
			// 	$res = updateRecord($table, "parent=:parent", "id=:id", ['id'=>$id, 'parent'=>$parent]);
			// 	break;
			// case 'spiritual':
			// 	$spiritual = json_encode($data->spiritual);
			// 	$res = updateRecord($table, "spiritual=:spiritual", "id=:id", ['id'=>$id, 'spiritual'=>$spiritual]);
			// 	break;
			default:				
				break;
		}
		
		echo $res ? json_encode(['ok' => 1]) : json_encode(['ok' => 0]);
		break;	
	case 'DELETE':		
		$id = $_SERVER['QUERY_STRING'];
		// $doc = selectSingleRecord($table, 'passport , certificate, biodata', "id='{$id}'");
		// $passport = $doc->passport;
		// $certificate = $doc->certificate;
		// $fdname = folder_name(json_decode($doc->biodata));

		extract(selectRecord($table, [], "id= :id", ['id'=>$id], "*"));

		$passport? @unlink('../' . $passport ) : '';
		
		$certificate? @unlink('../' . $certificate ) : '';
		
		@rmdir('../_online_uploads/set_2020/' . $fdname );

		$res = deleteRecord($table, "id=:id",['id'=>$id]) ;
		echo $res ? json_encode(['ok' => 1]) : json_encode(['ok' => 0]);
		break;
	default:
		break;
}

?>