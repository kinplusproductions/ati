<?php 
require_once('initialize.php');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';

$method = $_SERVER['REQUEST_METHOD'];
$table = 'applicants';
$table2 = 'referees';
$res = '';

switch($method){
	case 'GET':

		$u = isset($_GET['u']) ? $_GET['u'] : '' ;
		$count = isset($_GET['count']) ? $_GET['count'] : '' ;
		// $application_year = isset($_GET['a']) ? $_GET['a'] : '' ;
		$application_year = '2021' ;

		 if($u==2){
			if($count == 1){
				echo json_encode(countRecords($table));
			}else {
				echo json_encode(selectApplicants($table, "application_year=:application_year ORDER BY created_on ASC", ['application_year'=>$application_year]));
			}
		} else if($u==1){
			   echo json_encode(selectApplicant($table, "application_year=:application_year ORDER BY created_on ASC", ['application_year'=>$application_year]));
	   }
	    else{
			echo json_encode([]);
		} 
		// echo json_encode("You got here successfully");

		break;

	case 'POST':
		$data = json_decode(file_get_contents("php://input"), true); // Get raw posted data

	extract($data);
		$res2 = selectRecord($table, [], "email=:email AND phoneno=:phoneno", ['email' => $email, 'phoneno' => $phoneno], "*");
			if ($res2) {
				echo json_encode("A User With Email: $email and Phone Number: $phoneno Already Exist: Please Login To Continue Your Application");
				return;
				}
			
		$id = uuid();

		/* process application no */
		$setup = selectRecord('applicants_setup',[], "status=1",[]);
		$last_no = $setup['lastcode'];
		$yr = substr($setup['application_year'], -2);
		$last_no += 1;
		$application_no = $last_no < 10 ? "ATI{$yr}-0{$last_no}": "ATI{$yr}-{$last_no}"; // allocate application no
		$application_year = $setup['application_year'];
		$application_set = $setup['application_set'];
		updateRecord('applicants_setup', "lastcode=:last_no", "status=1", ['last_no' => $last_no]); // update last number
		$passwd = sha1($passwd);
		// application_year, application_set

		$column = "id,application_no,passwd,surname,other_names,phoneno,email,gender,dob,home_address,otown,rtown,ostate,rstate,ocountry,rcountry,marital_status,married_stay,health_challenges,application_year, application_set";

		$value = ":id, :application_no, :passwd, :surname, :other_names, :phoneno, :email, :gender, :dob, :home_address, :otown, :rtown, :ostate, :rstate, :ocountry, :rcountry, :marital_status, :married_stay, :health_challenges,:application_year, :application_set";

		$post_data=[
			'id'=>$id, 
			'application_no'=>$application_no, 'passwd'=>$passwd, 'surname'=>$surname, 'other_names'=>$other_names, 'phoneno'=>$phoneno, 'email'=>$email, 'gender'=>$gender, 'dob'=>$dob, 'home_address'=>$home_address, 'otown'=>$otown, 'rtown'=>$rtown, 'ostate'=>$ostate, 'rstate'=>$rstate, 'ocountry'=>$ocountry, 'rcountry'=>$rcountry, 'marital_status'=>$marital_status, 'married_stay'=>$married_stay, 'health_challenges'=>$health_challenges,'application_year'=>$application_year, 'application_set'=>$application_set];

		$res = insertRecord($table, $column, $value, $post_data);
			if ($res) {					
				// echo json_encode('inserted'); 
				echo json_encode(selectRecord($table,[], "id=:id", ['id' => $id])); 
			} else {
				echo json_encode('Unable to create new record, Please Try Again Later');				
			}	
		break;
	case 'PUT': 
	case 'PATCH':
		$data = json_decode(file_get_contents("php://input"), true); // Get raw posted data
		$mailsent='';
		$smsdata='';
		$res = '';
		extract($data);
		switch ($type) {
			case 'personal':				
				$column = "surname=:surname, other_names=:other_names, phoneno=:phoneno, email=:email, gender=:gender, dob=:dob, home_address=:home_address, otown=:otown, rtown=:rtown, ostate=:ostate, rstate=:rstate, ocountry=:ocountry, rcountry=:rcountry, marital_status=:marital_status, married_stay=:married_stay, health_challenges=:health_challenges";

				$update_data=['id'=>$id, 'surname'=>$surname, 'other_names'=>$other_names, 'phoneno'=>$phoneno, 'email'=>$email, 'gender'=>$gender, 'dob'=>$dob, 'home_address'=>$home_address, 'otown'=>$otown, 'rtown'=>$rtown, 'ostate'=>$ostate, 'rstate'=>$rstate, 'ocountry'=>$ocountry, 'rcountry'=>$rcountry, 'marital_status'=>$marital_status, 'married_stay'=>$married_stay, 'health_challenges'=>$health_challenges];

				$res = updateRecord($table, $column, "id=:id", $update_data);
			break;

			case 'parent':		
				$column = "parent_name=:parent_name, parent_address=:parent_address, parent_phoneno=:parent_phoneno, parent_email=:parent_email";

				$update_data=['id'=>$id, 'parent_name'=>$parent_name, 'parent_address'=>$parent_address, 'parent_phoneno'=>$parent_phoneno, 'parent_email'=>$parent_email];

				$res = updateRecord($table, $column, "id=:id", $update_data);
			break;
			case 'education':		
				$column = "previous_education=:previous_education, qualification=:qualification, discipline=:discipline";

				$update_data=['id'=>$id, 'previous_education'=>$previous_education, 'qualification'=>$qualification, 'discipline'=>$discipline];
				$res = updateRecord($table, $column, "id=:id", $update_data);
			break;
			case 'spiritual':		
				$column = "born_again=:born_again, salvation_experience=:salvation_experience, in_discipleship=:in_discipleship, discipler_name=:discipler_name, discipler_address=:discipler_address, discipler_phoneno=:discipler_phoneno, church=:church, mission_burdened=:mission_burdened, mission_burden_details=:mission_burden_details";

				$update_data=['id'=>$id, 'born_again'=>$born_again, 'salvation_experience'=>$salvation_experience, 'in_discipleship'=>$in_discipleship, 'discipler_name'=>$discipler_name, 'discipler_address'=>$discipler_address, 'discipler_phoneno'=>$discipler_phoneno, 'church'=>$church, 'mission_burdened'=>$mission_burdened, 'mission_burden_details'=>$mission_burden_details];

				$res = updateRecord($table, $column, "id=:id", $update_data);
			break;
			
			case 'uploads':		
				$column = "passport=:passport, certificate=:certificate";
				$update_data=['id'=>$id, 'passport'=>$passport, 'certificate'=>$certificate];
				$res = updateRecord($table, $column, "id=:id", $update_data);
			break;

			case 'declaration':		
				$res = updateRecord($table, "declaration=:declaration", "id=:id", ['id'=>$id, 'declaration'=>$declaration]);
			break;

			case 'shortlisted':		
				$res = updateRecord($table, "shortlisted=:shortlisted", "id=:id", ['id'=>$id, 'shortlisted'=>$shortlisted]);
			break;

			case 'interviewed':		
				$res = updateRecord($table, "interviewed=:interviewed", "id=:id", ['id'=>$id, 'interviewed'=>$interviewed]);
			break;

			case 'admitted':		
				$res = updateRecord($table, "admitted=:admitted", "id=:id", ['id'=>$id, 'admitted'=>$admitted]);
			break;

			case 'resumed':		
				$res = updateRecord($table, "resumed=:resumed", "id=:id", ['id'=>$id, 'resumed'=>$resumed]);
			break;

			case 'submitted':		
			$mail     = new PHPMailer();
			
			$res = updateRecord($table, "submitted=:submitted", "id=:id", ['id'=>$id, 'submitted'=>$submitted]);
			$applicant = selectRecord($table, [], "id=:id", ['id' => $id]);
			if ($res && $applicant['submitted']==1) {
					$referee = selectRecords($table2, [], "applicant_id=:id", ['id' => $applicant['id']]);
					$mailsent = '';
					$applicant=selectRecord($table,[], "id= :id", ['id'=>$id]);
					$mailreferee = send_double_referee_mail($mail,$applicant,$referee);
					if($mailreferee['mailsent']){
						$mailsent="Email Sent";
					}else{
						$mailsent="Email Failed: ". $mailreferee['message'];
					}			
				}
				break;					
			default:				
				break;
			}
			echo $res ? json_encode(['ok' => 1, 'mailsent' => $mailsent]) : json_encode(['ok' => 0]);
				
break;
	case 'DELETE':		
		$id = $_SERVER['QUERY_STRING'];
		$upload_path="../uploads/2021/";
		extract(selectRecord($table, [], "id= :id", ['id'=>$id], "*"));
		
		$passport ? @unlink($upload_path . $passport): '';
		$certificate ? @unlink($upload_path . $certificate): '';
			
		$res2 = deleteRecord($table2, "applicant_id=:id",['id'=>$id]) ;
		$res = deleteRecord($table, "id=:id",['id'=>$id]) ;

		echo ($res2 && $res) ? json_encode(['ok' => 1]) : json_encode(['ok' => 0]);
		break;
default:
	break;
}

?>