<?php 
require_once('initialize.php');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';

$mail     = new PHPMailer();
$app_no = 'ATI20-87';
$app_name = 'Applicant Name';
$app_email = 'atipeacehouse@gmail.com';
// $app_email = 'lekanojulowo@hotmail.com';
$rf1_name = 'Babatunde James';
$rf1_email = 'babatundejames22@gmail.com';

$rf2_name = 'Kolawole Ayoola';
$rf2_email = 'kolawoleayo12@gmail.com';

// $res = send_mail($mail, $app_no);

$host = 'mail.supremecluster.com';
	$username = 'admissions@ati.livingseed.org';
	$passwd   = 'seed@soa#19';
	$mailfrom = 'admissions@ati.livingseed.org';
	$sendername = 'Peace House Agricultural Training Institute';
	$ref_link = 'ati.livingseed.org/#/rf/';
	$img_link = 'ati.livingseed.org/';
	// $ref_link   = '../#/rf/';
	// $img_link = '../';
	// $mail->SMTPDebug = 3;                            // Enable verbose debug output

	$mail->isSMTP();                                      // Set mailer to use SMTP
	$mail->Host = $host;                                  // Specify main and backup SMTP servers
	$mail->SMTPAuth = true;   															 // Enable SMTP authentication
	$mail->SMTPKeepAlive = true;  // SMTP connection will not close after each email sent, reduces SMTP overhead
	$mail->Username = $username;                          // SMTP username
	$mail->Password = $passwd;                           // SMTP password
	$mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
	$mail->Port = 465;                                    // TCP port to connect to

	$mail->setFrom($mailfrom, $sendername);

	$mail->isHTML(true);                                  // Set email format to HTML

	$mail->Subject = "Reference Form Not Yet Filled By Your Referees";
	
	$mail->addAddress($app_email, ucwords($app_name));     // Add a recipient
	$mail->Body = '
		<!DOCTYPE html>
		<html lang="en">
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<style>
			.title{
				text-align: center;
				color: green;
			}
			.row{
				display: flex;
				justify-content: center;
				align-items: center;
				flex-wrap: wrap;
			}
			img {
				max-width: 100%;
			}
			body {
				margin-left: auto;
				margin-right: auto;
				padding-left: 1rem;
      }
      .link {
        color: blue;
      }
		</style>
		<title>Reference Form Not Filled</title>
		</head>
		<body>
		<div class="row">
			<img src="'.$img_link.'_images/ati-logo320x160.png" alt="ATI">
			<div class="title">
				<h4>Peace House <br>  Agricultural Training Institute <br>
				<small>Isarun, Ondo State, Nigeria</small></h4>
			</div> 
			<img src="'.$img_link.'_images/ls-logo.png" alt="Livingseed">
		</div>
		<h3>Dear '.ucwords($app_name).',</h3>

		<b>Application Number:</b> '. $app_no .' <br>

		<p>This is to inform you that both of your referees ('. $rf1_name.' - '.$rf1_email.')  and ('. $rf2_name.' - '.$rf2_email.') are yet to fill the reference form. Please confirm whether their emails are correct, or anything else that could be the reason and ensure the reference forms are submitted before Friday October 16th, 2020; otherwise your form will not be treated.</p>
		
		<h4><b>ATI Admission Team</b></h4>
			</body>
		</html>

    ';
		$mail->AltBody = "This is to inform you that both of your referees (". $rf1_name." - ".$rf1_email.")  and (". $rf2_name." - ".$rf2_email.") are yet to fill the reference form. Please confirm whether their emails are correct, or anything else that could be the reason and ensure the reference forms are submitted before Friday October 16th, 2020; otherwise your form will not be treated.";

    if(!$mail->send()) {
        $msg = 'Mail could not be sent.';
        $msg .= 'Mailer Error: ' . $mail->ErrorInfo;
        $mailsent = 0;
    } else {
      $msg = 'Mail Sent Successfully.';
      $mailsent = 1;
    }
    // echo $mail->Body;
		$res = ['mailsent'=>$mailsent, 'message'=>$msg];


print_r($res);
?>