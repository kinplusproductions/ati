<?php 
require_once('initialize.php');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';
$failed = [];
$failed_phoneno = [];
$failed_count = 0;
$success = [];
$success_phoneno = [];
$success_count = 0; 
$applicants = selectRecords('applicants', [], "shortlisted=1");

foreach ($applicants as $ap) {
$mail = new PHPMailer();
// $app_no = 'ATI21-01';
// $app_email = 'lekanojulowo@gmail.com';
// $app_name = 'Dearly Beloved';
// $app_email = 'atipeacehouse@gmail.com';
$app_name = $ap['surname'] . " ". $ap['other_names'] ;
$app_email = $ap['email'];
$app_phoneno = $ap['phoneno'];

// $res = send_mail($mail, $app_no);

$host = 'mail.supremecluster.com';
	$username = 'admissions@ati.livingseed.org';
	$passwd   = 'seed@soa#19';
	$mailfrom = 'admissions@ati.livingseed.org';
	$sendername = 'Peace House Agricultural Training Institute';
	$doc_link = 'https://ati.livingseed.org/docs/ati-2021-interview-letter.pdf';
	$img_link = 'https://ati.livingseed.org/img/';
	// $doc_link   = '../_docs/ati-2020-interview-letter.pdf';
	// $img_link = '../';
	// $mail->SMTPDebug = 3;                            // Enable verbose debug output

	$mail->isSMTP();                                      // Set mailer to use SMTP
	$mail->Host = $host;                                  // Specify main and backup SMTP servers
	$mail->SMTPAuth = true;   															 // Enable SMTP authentication
	$mail->SMTPKeepAlive = true;  // SMTP connection will not close after each email sent, reduces SMTP overhead
	$mail->Username = $username;                          // SMTP username
	$mail->Password = $passwd;                           // SMTP password
	$mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
	$mail->Port = 465;                                    // TCP port to connect to

	$mail->setFrom($mailfrom, $sendername);

	$mail->isHTML(true);                                  // Set email format to HTML

	$mail->Subject = "Invitation for Interview";
	
	$mail->addAddress($app_email, ucwords($app_name));     // Add a recipient
	$mail->Body = '
		<!DOCTYPE html>
		<html lang="en">
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<style>
			.title{
				text-align: center;
				color: green;
			}
			.row{
				display: flex;
				justify-content: center;
				align-items: center;
				flex-wrap: wrap;
			}
			img {
				max-width: 100%;
			}
			body {
				margin-left: auto;
				margin-right: auto;
				padding-left: 1rem;
      }
      .link {
        color: blue;
			}
			.text-center{
				text-align:center
			}
		</style>
		<title>Invitation for Interview</title>
		</head>
		<body>
		<div class="row">
			<img src="'.$img_link.'ati-logo320x160.png" alt="ATI">
			<div class="title">
				<h4>Peace House <br>  Agricultural Training Institute <br>
				<small>Isarun, Ondo State, Nigeria</small></h4>
			</div> 
			<img src="'.$img_link.'ls-logo.png" alt="Livingseed">
		</div>
		
		<h4 class="text-center">31st August, 2021</h4>
		
		<h4>Beloved,</h4>

		<p>Calvary greetings to you in the name of our Lord and Savior Jesus Christ.</p>
		
		<p>Sequel to your application to the Peace House Agricultural Training Institute (ATI), we like to inform you that you have been shortlisted for interview.</p>
		
		<h4><b><a href="'.$doc_link.'" download>Click Here to Download the Invitation Letter for Interview.</a></b></h4>

		<h4>Dr. (Mrs) P. A. Omololu.</h4>
		<h4>Director, ATI.</h4>
		</body>
		</html>

    ';
		$mail->AltBody = "Calvary greetings to you in the name of our Lord and Savior Jesus Christ. Sequel to your application to the Peace House Agricultural Training Institute (ATI) we like to inform you that you have been shortlisted for interview. Please click on this link: ".$doc_link." to download the invitation letter for interview.";
//  echo $mail->Body;
    if(!$mail->send()) {
        // $msg = 'Mail could not be sent.';
        // $msg .= 'Mailer Error: ' . $mail->ErrorInfo;
				// $mailsent = 0;
				$failed[]=$app_email;
				$failed_phoneno[]=$app_phoneno;
				$failed_count++;
    } else {
      // $msg = 'Mail Sent Successfully.';
			// $mailsent = 1;
			$success[]=$app_email;
			$success_phoneno[]=$app_phoneno;
			$success_count++;
    }
    // echo $mail->Body;
		// $res = ['mailsent'=>$mailsent, 'message'=>$msg];
}
$output = json_encode(['failed'=>$failed,'failed_phoneno'=>$failed_phoneno, 'failed_count'=>$failed_count, 'success'=>$success, 'success_phoneno'=>$success_phoneno, 'success_count'=>$success_count]);
print_r($output);
?>