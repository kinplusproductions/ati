<?php 
require_once('initialize.php');

$method = $_SERVER['REQUEST_METHOD'];
$table = 'school_info';
$json_fields = [];
$res = '';
switch($method){
	case 'GET':
		$u = isset($_GET['u']) ? $_GET['u'] : '' ;
		if($u==2){
			echo json_encode(selectRecords($table, $json_fields));
		}
		break;
	case 'POST':
		$data = json_decode(file_get_contents("php://input"), true); // Get raw posted data
		$id = $data['id'] = uuid();
		$column = "id,institute_address,institute_name,director_name,director_sign";
		$value = ":id, :institute_address, :institute_name, :director_name, :director_sign";
    $res = insertRecord($table, $column, $value, $data);
    echo $res ? json_encode(selectRecord($table, $json_fields, "id=:id", ['id' => $id])) : json_encode(['ok' => 0]);			
		break;
	case 'PUT': 
	case 'PATCH': 
		$data = json_decode(file_get_contents("php://input")); // Get raw posted data
		$id = $data->id;
		$res = '';
		$column = "institute_address=:institute_address,
				institute_name=:institute_name,
				director_name=:director_name,
				director_sign=:director_sign";

				$update_data=[
				'id'=>$id,
				'institute_address'=>$data->institute_address,
				'institute_name'=>$data->institute_name,
				'director_name'=>$data->director_name,
				'director_sign'=>$data->director_sign
				];
		$res = updateRecord($table, $column, "id=:id", $update_data);
		echo $res ? json_encode(['ok' => 1]) : json_encode(['ok' => 0]);		
		break;	
	case 'DELETE':		
		$id = $_SERVER['QUERY_STRING'];
		$res = deleteRecord($table, "id=:id",['id'=>$id]) ;
		echo $res ? json_encode(['ok' => 1]) : json_encode(['ok' => 0]);
		break;
	default:
		break;
}

?>