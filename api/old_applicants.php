<?php 
require_once('initialize.php');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';

$mail     = new PHPMailer();


$method = $_SERVER['REQUEST_METHOD'];
$table = 'napplicants';
$res = '';

switch($method){
	case 'GET':
		$u = isset($_GET['_u']) ? $_GET['_u'] : '' ;
		if($u==1){
			// echo json_encode(selectSingleRecord($table, '*', "1 ORDER BY created_on ASC"));
		}
		else if($u==2){
			echo json_encode(selectApplicants($table, '*', "1 ORDER BY created_on ASC"));
		}
	/* 	else {
			echo json_encode(selectRecords($table, '*', "1 ORDER BY created_on ASC"));
			// print_r(selectRecords($table, '*', "1 ORDER BY created_on ASC"));
		} */
		break;
	case 'POST':
		$data = json_decode(file_get_contents("php://input")); // Get raw posted data
		$id = $data->id;
		$app_no= $data->application_no;
		$submitted = $data->submitted;
		$already_submitted = $data->already_submitted;
		$account = sanitize_string(json_encode($data->account));
		$action = sanitize_string(json_encode($data->action));
		$biodata = sanitize_string(json_encode($data->biodata));
		$education = sanitize_string(json_encode($data->education));
		$parent = sanitize_string(json_encode($data->parent));
		$referee = sanitize_string(json_encode($data->referee));
		$spiritual = sanitize_string(json_encode($data->spiritual));

		$setup = selectSingleRecord('applicants_setup', '*', "status=1"); // fetch active setup
		$application_year = $setup->application_year; // set applicant's application year
		$application_set = $setup->application_set;	// set applicant's application set
		$res = '';

		if($id){
			$column = "account='$account', action='$action', biodata='$biodata', education='$education',  parent='$parent', referee='$referee', spiritual='$spiritual', submitted=$submitted";
			$where_clause = "id='{$id}'";
			$res = updateRecord($table, $column, $where_clause);
			
			//send mail to referees
			if($submitted == 1 && !$already_submitted){
			$maildata = send_double_referee_mail($mail, $data, $data->referee);
			$mailsent = $maildata['mailsent'];
			$msg = $maildata['message'];
			echo $res && $mailsent ? json_encode(selectApplicant($table, '*', $where_clause)) : $msg;
			} else{
				echo $res ? json_encode(selectApplicant($table, '*', $where_clause)) : 'No changes to update';
			}
		} else{
			$user_already_exists = user_exists($table, $data->biodata);
			if(!$user_already_exists){	
			$raw_passwd = $data->passwd;
			$passwd = sha1($data->passwd);
			$last_no = $setup->lastcode;
			$yr = substr($setup->application_year, -2);
			$last_no += 1;
			$app_no = $last_no < 10 ? "ATI{$yr}-0{$last_no}": "ATI{$yr}-{$last_no}"; // allocate application no
			$column = "id, application_no,passwd, account, action, biodata, education,  parent, referee, spiritual, application_year, application_set";
			$value = "(uuid(),'$app_no','$passwd','$account','$action', '$biodata', '$education', '$parent', '$referee', '$spiritual', '$application_year', $application_set)";
			$res = insertRecord($table, $column, $value); 
			updateRecord('applicants_setup', "lastcode=$last_no", "status=1 LIMIT 1");
			echo $res ? json_encode(selectApplicant($table, '*', "application_no='$app_no'")) : 'Unable to start application';
			} else {
				echo $user_already_exists;
			}
		}	
		break;
	case 'PUT': 
		$data = json_decode(file_get_contents("php://input")); // Get raw posted data
		$id = $data->id;
		$res = '';
		switch ($data->type) {
			case 'action':
				$action = sanitize_string(json_encode($data->action));
				$res = updateRecord($table, "action='{$action}'", "id='{$id}'");
				break;
			case 'referee':
				$referee = sanitize_string(json_encode($data->referee));
				$res = updateRecord($table, "referee='{$referee}'", "id='{$id}'");
				break;
			
			default:				
				break;
		}
		
		echo $res ? json_encode(['ok' => 1]) : json_encode(['ok' => 0]);
		break;	
	case 'DELETE':		
		$id = $_SERVER['QUERY_STRING'];
		$doc = selectSingleRecord($table, 'passport , certificate, biodata', "id='{$id}'");
		$passport = $doc->passport;
		$certificate = $doc->certificate;
		$fdname = folder_name(json_decode($doc->biodata));
		if($passport){
			@unlink('../' . $passport );
		}
		if($certificate){
			@unlink('../' . $certificate );
		}
		@rmdir('../_online_uploads/set_2020/' . $fdname );

		$res = deleteRecord($table, "id='{$id}'") ;
		echo $res ? json_encode(selectApplicants($table, '*', "1 ORDER BY created_on ASC")) : 'Unable to delete record';
		break;
	default:
		break;
}

?>