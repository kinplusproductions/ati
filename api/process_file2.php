<?php 
	require_once('initialize.php');
	$table = 'napplicants';
	$res = '';

	$data = json_decode($_POST['data']);
	$id = $data->id;
	$app_no= $data->application_no;
	$certificate = $data->certificate;
	$passport = $data->passport;
	$certfile = isset($_FILES['certificateFile']['name']);
	$passfile = isset($_FILES['passportFile']['name']);
	$biodata = $data->biodata;

	$year = selectSingleRecord('applicants_setup', 'application_year', "status=1")->application_year;
	$fdname = folder_name($biodata); // create applicant folder name
	$upload_path="_online_uploads/set_{$year}/{$fdname}";
	
	if($certfile){		
		if($certificate){
			@unlink('../' + $certificate);
		}
		$certname = strtolower("{$app_no}_certificate");	// create applicant certificate name
		$certificate = upload_file($_FILES['certificateFile']['name'], $_FILES['certificateFile']['tmp_name'], $upload_path, $certname);
	}

	if($passfile){
		if($passport){
			@unlink('../' + $passport);
		}
		$passname = strtolower("{$app_no}_passport");	// create applicant passport name
		$passport = upload_file($_FILES['passportFile']['name'], $_FILES['passportFile']['tmp_name'], $upload_path, $passname);
	}

	$column = "certificate='{$certificate}', passport='{$passport}'";
	$where_clause = "id='{$id}'";
	@updateRecord($table, $column, $where_clause);
	
	echo json_encode(['certificate' => $certificate, 'passport' => $passport]);
			
?>