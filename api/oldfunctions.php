<?php

function redirect_to($location=NULL){
  if($location != NULL){
    header("Location: {$location}");
    exit;
  }
}

function confirm_query($result){
  global $con;
	if(!$result){
	die("Database query failed: ". mysqli_error($con));
	}
}

function sanitize_string($string){
  global $con;
	return mysqli_real_escape_string($con, $string);
}

function selectSingleRecord($table, $column="*", $where_clause=1){
  global $pdo;
  $sql = "SELECT $column FROM $table WHERE $where_clause LIMIT 1";
  $stmt = $pdo->prepare($sql);
  $stmt ->execute();
  $row = $stmt->fetch();
  return $row ? $row : false;
}

// function selectRecords($table, $column="*", $where_clause=1){
//   global $pdo;
//   $sql = "SELECT $column FROM $table WHERE $where_clause ";
//   $stmt = $pdo->prepare($sql);
//   $stmt ->execute();
//   $row = $stmt->fetchAll();
//   return $row ? $row : [];
// }

function selectApplicant($table, $column="*", $where_clause=1){
  global $pdo;
  $sql = "SELECT $column FROM $table WHERE $where_clause LIMIT 1";
  $stmt = $pdo->prepare($sql);
	$stmt ->execute();
	$row = false;
  if($row = $stmt->fetch()){
    $row->account = json_decode($row->account);
    $row->biodata = json_decode($row->biodata);
    $row->education = json_decode($row->education);
    $row->parent = json_decode($row->parent);
    $row->referee = json_decode($row->referee);
    $row->spiritual = json_decode($row->spiritual);
    $row->action = json_decode($row->action);
  }

  return $row;
}

function decodeJson($record, $json_arr){
  foreach ($json_arr as $field) {
    $record[$field] = json_decode($record[$field]);
  }
  return $record;
}

function selectRecords($table, $json_arr=[], $where_clause=1, $data=[], $column="*"){
  global $pdo;
  $sql = "SELECT $column FROM $table WHERE $where_clause ";
  $stmt = $pdo->prepare($sql);
  $stmt->execute($data);
	$res = [];
  while($row = $stmt->fetch()){
    $res[] = decodeJson($row, $json_arr);
  }
	return $res;
}

function selectRecord($table, $json_arr=[], $where_clause=1, $data=[], $column="*"){
  global $pdo;
  $sql = "SELECT $column FROM $table WHERE $where_clause LIMIT 1";
  $stmt = $pdo->prepare($sql);
  $stmt->execute($data);
	$res = $stmt->fetch();
	return decodeJson($res, $json_arr);
}
function encodeJson($record, $json_arr){
  foreach ($json_arr as $field) {
    $record[$field] = json_encode($record[$field]);
  }
  return $record;
}
function uuid() {
	return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',

		// 32 bits for "time_low"
		mt_rand(0, 0xffff), mt_rand(0, 0xffff),

		// 16 bits for "time_mid"
		mt_rand(0, 0xffff),

		// 16 bits for "time_hi_and_version",
		// four most significant bits holds version number 4
		mt_rand(0, 0x0fff) | 0x4000,

		// 16 bits, 8 bits for "clk_seq_hi_res",
		// 8 bits for "clk_seq_low",
		// two most significant bits holds zero and one for variant DCE1.1
		mt_rand(0, 0x3fff) | 0x8000,

		// 48 bits for "node"
		mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
	);
}
function insertRecord($table, $column, $value, $data=[]){
  global $pdo;
  $sql ="INSERT INTO $table($column) VALUES ($value) ";
  $stmt = $pdo->prepare($sql);
	return $stmt->execute($data); 
}




function selectApplicants($table, $column="*", $where_clause=1){
  global $pdo;
  $sql = "SELECT $column FROM $table WHERE $where_clause ";
  $stmt = $pdo->prepare($sql);
  $stmt ->execute();
    $result = [];
  while($row = $stmt->fetch()){
    $row->account = json_decode($row->account);
    $row->biodata = json_decode($row->biodata);
    $row->education = json_decode($row->education);
    $row->parent = json_decode($row->parent);
    $row->referee = json_decode($row->referee);
    $row->spiritual = json_decode($row->spiritual);
    $row->action = json_decode($row->action);
    $result[] = $row;
  }

  return $result;
}

function selectSetupData($table, $column="*", $where_clause=1){
  global $pdo;
  $sql = "SELECT $column FROM $table WHERE $where_clause LIMIT 1";
  $stmt = $pdo->prepare($sql);
	$stmt ->execute();
	$row = false;
  if($row = $stmt->fetch()){
    $row->countries = json_decode($row->countries);
  }

  return $row;
}

function selectReference($table, $column="*", $where_clause=1){
  global $pdo;
  $sql = "SELECT $column FROM $table WHERE $where_clause LIMIT 1";
  $stmt = $pdo->prepare($sql);
  $stmt ->execute();
    $row = false;
  if($row = $stmt->fetch()){
    $row->ref = json_decode($row->ref);
	}
	
  return $row;
}

function selectReferences($table, $column="*", $where_clause=1){
  global $pdo;
  $sql = "SELECT $column FROM $table WHERE $where_clause ";
  $stmt = $pdo->prepare($sql);
  $stmt ->execute();
    $result = [];
  while($row = $stmt->fetch()){
    $row->ref = json_decode($row->ref);
    $result[] = $row;
  }

  return $result;
}

/* function insertRecord($table, $column, $value){
  global $con;
  $query ="INSERT INTO $table($column) VALUES $value ";
  $result = mysqli_query($con,$query);
  confirm_query($result);
  return $result ? true: false; 
} */

function edit_table(){
	
	$sql = "ALTER TABLE `courses` CHANGE `id` `id` VARCHAR(40) NOT NULL, CHANGE `year1sem1` `course_name` VARCHAR(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL, CHANGE `year1sem2` `course_code` VARCHAR(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL, CHANGE `year2sem1` `course_unit` INT(5) NULL DEFAULT NULL, CHANGE `year2sem2` `level` VARCHAR(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL, CHANGE `created_on` `created_on` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP";
}

function updateRecord($table, $column, $where_clause){
  global $con;
  $query = "UPDATE $table SET $column WHERE $where_clause ";
  $result = mysqli_query($con,$query);
  confirm_query($result);
  return mysqli_affected_rows($con);	
}

function deleteRecord($table, $where_clause){
  global $con;
  $query = "DELETE FROM $table WHERE $where_clause LIMIT 1 ";
  $result = mysqli_query($con,$query);
  confirm_query($result);
  return mysqli_affected_rows($con);
}

function truncateRecord($table){
  global $con;
  $query = "TRUNCATE TABLE $table";
  $result = mysqli_query($con,$query);
  confirm_query($result);
  return true;
}

function mail_referee($mail, $applicant, $referee){
	$app_no = $applicant->application_no;
	$bd = $applicant->biodata;
	$fname  = (strtoupper($bd->surname .', '. $bd->other_names));

	$rf_id = $referee->id;
	$rf_name = $referee->name;
	$rf_email = $referee->email;
		
			
		

	$host = 'mail.supremecluster.com';
	$username = 'admissions@ati.livingseed.org';
	$passwd   = 'seed@soa#19';
	$mailfrom = 'admissions@ati.livingseed.org';
	$sendername = 'Peace House Agricultural Training Institute';
	$ref_link = 'ati.livingseed.org/#/rf/';
	$img_link = 'ati.livingseed.org/';
	// $ref_link   = '../#/rf/';
	// $img_link = '../';
	// $mail->SMTPDebug = 3;                            // Enable verbose debug output

	$mail->isSMTP();                                      // Set mailer to use SMTP
	$mail->Host = $host;                                  // Specify main and backup SMTP servers
	$mail->SMTPAuth = true;   															 // Enable SMTP authentication
	$mail->SMTPKeepAlive = true;  // SMTP connection will not close after each email sent, reduces SMTP overhead
	$mail->Username = $username;                          // SMTP username
	$mail->Password = $passwd;                           // SMTP password
	$mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
	$mail->Port = 465;                                    // TCP port to connect to

	$mail->setFrom($mailfrom, $sendername);

	$mail->isHTML(true);                                  // Set email format to HTML

	$mail->Subject = "Reference requested for {$fname} ({$app_no})";
	
	$mail->addAddress($rf_email, ucwords($rf_name));     // Add a recipient
	$mail->Body = '
		<!DOCTYPE html>
		<html lang="en">
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<style>
			.title{
				text-align: center;
				color: green;
			}
			.row{
				display: flex;
				justify-content: center;
				align-items: center;
				flex-wrap: wrap;
			}
			img {
				max-width: 100%;
			}
			body {
				margin-left: auto;
				margin-right: auto;
				padding-left: 1rem;
      }
      .link {
        color: blue;
      }
		</style>
		<title>Reference Request for '.$fname.'</title>
		</head>
		<body>
		<div class="row">
			<img src="'.$img_link.'_images/ati-logo320x160.png" alt="ATI">
			<div class="title">
				<h4>Peace House <br>  Agricultural Training Institute <br>
				<small>Isarun, Ondo State, Nigeria</small></h4>
			</div> 
			<img src="'.$img_link.'_images/ls-logo.png" alt="Livingseed">
		</div>
		<h3>Dear '.ucwords($rf_name).',</h3>

		<h4>Applicant Details</h4>
		<b>Applicant Name:</b> '.$fname.' <br>
		<b>Application Number:</b> '. $app_no .' <br>

		<p>This is a request from The Peace House Agricultural Training Institute (ATI, Isarun, Ondo State, Nigeria.</p> 
		<p><b>'. $fname .'</b> has recently made an application to the institute for a comprehensive vocational agricultural training, and has named you as a referee. </p>
		<p>When considering whether to make an offer of place to a particular applicant, our Admissions Team values the opinion of a referee with experience of the applicant.</p> 
		<p>We would, therefore, be very grateful if you would please provide us with a reference for the above applicant, commenting on his/her suitability to undertake the training.</p>
		<p>Please comment on the applicant with all sincerity and faithfulness to God.
		Please make use of the space provided in the following link to give information about the applicant.</p>
		<p><b><a href="'.$ref_link.$app_no.'/'.$rf_id.'">Click here to fill the reference form</a></b></p>
		<p>Alternatively you can copy and paste the following link into your browser directly:  <b class="link">'.$ref_link.$app_no.'/'.$rf_id.'</b></p>
		May I take this opportunity to thank you in advance for your time in preparing this reference.

		<h4><b>ATI Admissions Team</b></h4>
			</body>
		</html>

    ';
		$mail->AltBody = "There is a reference request from The Peace House Agricultural Training Institute (ATI), Isarun, Ondo State. Please follow this link: ".$ref_link.$app_no.'/'.$rf_id." to complete the reference application.";

    if(!$mail->send()) {
        $msg = 'Mail could not be sent.';
        $msg .= 'Mailer Error: ' . $mail->ErrorInfo;
        $mailsent = 0;
    } else {
      $msg = 'Mail Sent Successfully.';
      $mailsent = 1;
    }
    // echo $mail->Body;
		return ['mailsent'=>$mailsent, 'message'=>$msg];
	}

  
function send_double_referee_mail($mail, $applicant, $referee){
	$res = mail_referee($mail, $applicant, $referee[0]);
	$res = mail_referee($mail, $applicant, $referee[1]);

	// foreach($ref as $rf){
	// 	$res = mail_referee($mail, $app_no, $rf->id);
	// }
	return $res;
}
  
  function upload_file($image_name, $tmp_dir, $upload_path, $fname){
	@mkdir("../{$upload_path}");
  $extension = pathinfo($image_name, PATHINFO_EXTENSION);
  $filename = "{$upload_path}/{$fname}.{$extension}";
  $filepath = "../{$filename}";	
  @move_uploaded_file($tmp_dir, $filepath);
    return $filename;
}

function folder_name($bd){
	$othername1 = explode(" ", $bd->other_names);
	$othername2 = implode("_", $othername1);
return strtolower($bd->surname . '_' . $othername2); 
}

function user_exists($table, $bd1){
	$applicants = selectRecords($table, "biodata, application_no, submitted");
	$res = false;
		foreach ($applicants as $a) {
			$bd2 = json_decode($a->biodata);
			if(($bd1->surname == $bd2->surname && $bd1->other_names == $bd2->other_names) || ($bd1->mobile_no == $bd2->mobile_no) || ($bd1->email == $bd2->email)){
				$res = json_encode(['biodata' => $a->biodata, 'application_no' =>$a->application_no, 'submitted' => $a->submitted]);
				break;
			}
		}
	return $res;
}


?>

