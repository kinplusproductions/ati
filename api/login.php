<?php 
require_once('initialize.php');

$data = json_decode(file_get_contents("php://input"), true); // Get raw posted data
extract($data);
$passwd = sha1($passwd);
$table = "applicants";
$msg = '';
$res = '';

$applicant = selectRecord($table, [], "application_no=:app_no", ['app_no'=>$app_no]);
if($applicant){
	if($passwd == $applicant['passwd']){
		$res = selectApplicant($table, "application_no=:app_no", ['app_no'=>$app_no]);
	}else{
		$msg = "Incorrect Password: Please Enter a Correct Password";
	}
}else{
		$msg = "Incorrect Application Number: Please Enter Correct Detail.";
	}
	echo $res ? json_encode($res) : json_encode($msg);

?>