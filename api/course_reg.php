<?php 
require_once('initialize.php');

$method = $_SERVER['REQUEST_METHOD'];
$table = 'course_reg';
$json_fields = ['course_ids'];
$res = '';
switch($method){
	case 'GET':
		$u = isset($_GET['u']) ? $_GET['u'] : '' ;
		$student_id = isset($_GET['std_id']) ? $_GET['std_id'] : '' ;
		$cur_session = isset($_GET['ses']) ? $_GET['ses'] : '' ;
		$level = isset($_GET['l']) ? $_GET['l'] : 0 ;
		$semester = isset($_GET['s']) ? $_GET['s'] : 0 ;
		if($u==1){
			$where_clause = "student_id=:student_id AND cur_session=:cur_session AND level=:level AND semester=:semester";
			$data = ['cur_session'=>$cur_session, 'student_id'=>$student_id, 'semester'=>$semester, 'level'=>$level];
			$res = selectRecord($table, $json_fields, $where_clause , $data);
			echo	$res ? json_encode($res) : 'No course registered yet';
		}
		else if($u==2){
			$table2 = "students";
			$column = "t1.course_ids, t2.id, t2.application_no, t2.surname, t2.other_names";
			$on_clause = "t1.student_id=t2.id";
			$where_clause = "t1.cur_session=:cur_session AND t1.level=:level AND t1.semester=:semester ORDER BY t2.application_no ASC";
			$data = ['cur_session'=>$cur_session, 'level'=>$level, 'semester'=>$semester];
			echo json_encode(joinRecords($table, $table2,$column, $on_clause, $where_clause, $json_fields, $data));
		}
		break;
	case 'POST':
		$data = json_decode(file_get_contents("php://input"), true); // Get raw posted data
		$id = $data['id'] = uuid();
		$data = encodeJson($data,$json_fields);
		$column = "id,student_id,cur_session,course_ids,level,semester";
		$value = ":id, :student_id, :cur_session, :course_ids, :level, :semester";
    $res = insertRecord($table, $column, $value, $data);
    echo $res ? json_encode(selectRecord($table, $json_fields, "id=:id", ['id' => $id], '*')) : json_encode(['ok' => 0]);			
		break;
	case 'PUT': 
	case 'PATCH': 
		$data = json_decode(file_get_contents("php://input")); // Get raw posted data
		$id = $data->id;
		$res = '';
		$course_ids = json_encode($data->course_ids);
		$res = updateRecord($table, "course_ids=:course_ids", "id=:id", ['id'=>$id, 'course_ids'=>$course_ids]);
		echo $res ? json_encode(['ok' => 1]) : json_encode(['ok' => 0]);
		break;	
	case 'DELETE':		
		$id = $_SERVER['QUERY_STRING'];
		$res = deleteRecord($table, "id=:id",['id'=>$id]) ;
		echo $res ? json_encode(['ok' => 1]) : json_encode(['ok' => 0]);
		break;
	default:
		break;
}

?>