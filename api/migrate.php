<?php 
require_once('initialize.php');

$data = json_decode(file_get_contents("php://input")); // Get raw posted data
$ref = $data->ref;
$table = "napplicants";
foreach ($ref as $q) {
	$id = $q->id;	
	$rf = json_encode($q->referee);
	$column = "referee='{$rf}'";
	$where_clause = "id='{$id}'";
	$res = updateRecord($table, $column, $where_clause);
}
echo json_encode(['ok'=>true]);
?>